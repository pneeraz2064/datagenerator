/**
 * This helper module should open the relevant file and append the data to it.
 * Note: The file handler needs to be imported only once in the index file and passed 
 * to the functions that might need it
 */
const fs = require('fs');
const csv_reader = require('csv-reader');

const OPEN_FILES = {};
let FOLDER_NAME;

const refreshFolderName = () => {
    const today = new Date();
    FOLDER_NAME = `${today.getFullYear()}-${today.getMonth()}-${today.getDate()}/${today.getHours()}.${today.getMinutes()}.${today.getSeconds()}`;
    initFolder();
};

const initFolder = () => {
    if (!fs.existsSync(`./FILES/${FOLDER_NAME}`)) {
        fs.mkdirSync(`./FILES/${FOLDER_NAME}`, { recursive: true });
    }
}

const openFile = (salesforceObjectName, isReadOnly = false, csvHeader = '', callback) => {
    if (OPEN_FILES[salesforceObjectName]) return;

    if (isReadOnly) {
        OPEN_FILES[salesforceObjectName] = {
            stream: [],
            isReadOnly,
            completed: false
        };
        try {
            fs.createReadStream(`./files/readonly/${salesforceObjectName}.csv`, 'utf8')
                .pipe(new csv_reader({
                    skipHeader: true,
                    asObject: true,
                    allowQuotes: true
                })).on('data', function (row) {
                    OPEN_FILES[salesforceObjectName].stream.push(row);
                })
                .on('end', function (data) {
                    OPEN_FILES[salesforceObjectName].completed = true;
                    if (callback) callback(null);
                });
        } catch (err) {
            if (callback) callback(err);
        }
        return;
    }

    OPEN_FILES[salesforceObjectName] = {
        stream: fs.createWriteStream(`./files/${FOLDER_NAME}/${salesforceObjectName}.csv`, { flags: 'a+' }),
        isReadOnly
    }
    appendToFile(salesforceObjectName, csvHeader)
}

const getOpenFile = salesforceObjectName => {
    return OPEN_FILES[salesforceObjectName];
}

const appendToFile = (salesforceObjectName, dataToAppend) => {
    if (OPEN_FILES[salesforceObjectName] && !OPEN_FILES[salesforceObjectName].isReadOnly) {
        OPEN_FILES[salesforceObjectName].stream.write(dataToAppend);
        return true;
    }
    return false;
}

const closeFile = salesforceObjectName => {
    if (OPEN_FILES[salesforceObjectName]) {
        if (!OPEN_FILES[salesforceObjectName].isReadOnly) OPEN_FILES[salesforceObjectName].stream.end();
        delete OPEN_FILES[salesforceObjectName];
    }
}

const closeAllFiles = () => {
    for (const key of Object.keys(OPEN_FILES)) {
        OPEN_FILES[key].isReadOnly ? delete OPEN_FILES[key] : closeFile(key);
    }
}

module.exports = {
    initFolder,
    openFile,
    getOpenFile,
    appendToFile,
    closeFile,
    closeAllFiles,
    refreshFolderName
}