/**
 * This helper file will store all sorts of functions necessary to work with the data
 */

const formatData = (dataToExtract, objectWithData) => {
    return `"${dataToExtract.map(field => objectWithData[field] || '').join('","')}"\n`;
}

const createHeader = dataToExtract => {
    return dataToExtract.join(',') + '\n';
}

const findConfig = (config = null, configPath = []) => {
    if (!config || configPath.length == 0) return null;
    if (config[configPath[0]] && configPath.length == 1) return config[configPath[0]];
    return findConfig(config[configPath[0]], configPath.slice(1));
}

const filterArrayData = (filterFrom = [], filter = [], allowed = true) => {
    if (!Array.isArray(filterFrom)) return filterFrom;
    return filterFrom.filter(value => {
        return allowed ? filter.includes(value) : !filter.includes(value);
    });
}

const findFromCSV = (CSVdata, dataToFind, equalsValue) => {
    return CSVdata.find(CSVObject => CSVObject[dataToFind] == equalsValue);
}

module.exports = {
    formatData,
    createHeader,
    findConfig,
    filterArrayData,
    findFromCSV
}