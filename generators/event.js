const faker = require('faker');
const { formatData, findConfig, filterArrayData, findFromCSV } = require('../helpers/data_helper');
const { EVENT, EVENT_FIELD_LIST } = require('../const');
//picklists
const EVENT_SUBJECT = ['Employee Meeting', 'Volunteering', 'Client Meeting', 'Conference/Travel', 'Webinar', 'Executive Team Meeting', 'Client Appreciation/Engagement'];
const EVENT_TYPE = ['Email', 'Meeting', 'Other', 'Call'];

const generate = (config, fileHandler, objectType, contactLead, entityAssetLiabProcessPolCov) => {
    let activityDate = faker.date.between('10/07/2020', '12/12/2022');
    let DurationInMinutes = faker.datatype.number({ min: 10, max: 17280 });
    const event = {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "IsAllDayEvent": faker.datatype.boolean(),
        "ActivityDate": activityDate.toISOString().split('T')[0],
        "StartDateTime": activityDate.toISOString(),
        "ActivityDateTime": activityDate.toISOString(),
        "Description": objectType + faker.lorem.words(50),
        "EndDateTime": new Date(activityDate.setMinutes(activityDate.getMinutes() + DurationInMinutes)).toISOString(),
        "Email": faker.internet.email(),
        "Location": faker.address.streetAddress() + ', ' + faker.address.city(),
        "WhoId": contactLead, //(contact and lead)
        "Phone": faker.phone.phoneNumber(),
        "IsPrivate": false,
        "WhatId": entityAssetLiabProcessPolCov, //vvimp (Entity, Asset/Liability, Deal, Process, Policy Coverage)
        "IsReminderSet": faker.datatype.boolean(),
        "Subject": faker.random.arrayElement(EVENT_SUBJECT),
        "Type": faker.random.arrayElement(EVENT_TYPE),
        "Who.Id": contactLead,
        "What.Id": entityAssetLiabProcessPolCov,
        // "Account.Id": whatId
    }

    //save the Event
    console.log('----Event---');
    fileHandler.appendToFile(EVENT, formatData(EVENT_FIELD_LIST, event));
}

module.exports = generate;

/**
IsAllDayEvent=> Checkbox
ActivityDate=> Date/Time
Description=> Long Text Area(32000)
DurationInMinutes=> Number(8, 0)
Email=> Email
EndDateTime=> Date/Time
Location=> Text(255)
NAME, WhoId=> Lookup(Contact,Lead)
Phone=> Phone
IsPrivate=> Checkbox
RELATED TO WhatId=> Lookup(Contract,Order,Campaign,Entity,Opportunity,Product,Asset,Case,Solution,Work Order,Work Order Line Item,Entitlement,Service Contract,Contract Line Item,Location,Asset Relationship,List Email,Contact Request,Image,Communication Subscription Consent,Party Consent,Process Exception,Asset/Liability,Deal,Division,Process,Service,Financial Product,Envestnet Proposal,Plan,Policy Coverage,Team Member,Goal,Income/Expense,Policy)
IsReminderSet=> Checkbox
StartDateTime=> Date/Time
Subject=> Picklist
ActivityDateTime=> Date/Time
Type=> Picklist


if the event is private the what id cannot be populated
 */