const faker = require('faker');
const { formatData, findConfig, filterArrayData, findFromCSV } = require('../helpers/data_helper');
const { TASKS, TASK_FIELD_LIST } = require('../const');

//picklists
const TASK_PRIORITY = ['High', 'Normal', 'Low'];
const TASK_STATUS = ['Not Started', 'In Progress', 'Completed', 'Waiting on someone else', 'Deferred'];
const TASK_SUBJECT = ['Send Documents', 'Send Email', 'Schedule Meeting', 'Call', 'Send Collateral', 'Review', 'Follow-Up', 'Execute Trade', 'Send Text/SMS'];
const TASK_TYPE = ['Call', 'Meeting', 'Other', 'Email'];

const generate = (config, fileHandler, objectType, contactLead, entityAssetLiabProcessPolCov) => {
    const task = {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "CallDurationInSeconds": faker.datatype.number(500),
        "Description": objectType + faker.lorem.words(50),
        // "IsRecurrence": faker.datatype.boolean(), //Apex Task trigger cannot handle batch operations on recurring tasks.
        "ActivityDate": faker.date.between('10/07/2020', '12/12/2022').toISOString().split('T')[0],
        "Email": faker.internet.email(),
        "WhoId": contactLead, //(contact and lead)
        "Phone": faker.phone.phoneNumber(),
        "Priority": faker.random.arrayElement(TASK_PRIORITY),
        "IsVisibleInSelfService": faker.datatype.boolean(),
        // "RecurrenceInterval": faker.datatype.boolean() ? faker.datatype.number(20) : 0,
        "WhatId": entityAssetLiabProcessPolCov, //vvimp (Entity, Asset/Liability, Deal, Process, Policy Coverage)
        // "AccountId": entityAssetLiabProcessPolCov,
        "IsReminderSet": faker.datatype.boolean(),
        "Status": faker.random.arrayElement(TASK_STATUS),
        "Subject": faker.random.arrayElement(TASK_SUBJECT),
        "Type": faker.random.arrayElement(TASK_TYPE),
        "Who.Id": contactLead,
        "What.Id": entityAssetLiabProcessPolCov,
        // "Account.Id": whatId
    }
    //save the activity
    console.log('----Task---');
    fileHandler.appendToFile(TASKS, formatData(TASK_FIELD_LIST, task));
}

module.exports = generate;