const faker = require('faker');
const { formatData, findConfig, filterArrayData, findFromCSV } = require('../helpers/data_helper');
const { POLICY_COVERAGE, POLICY_COVERAGE_FIELD_LIST, DATES_START_END } = require('../const');
const policyGenerator = require('./policy');
const taskGenerator = require('./task');
const eventGenerator = require('./event');

//picklists
const POLICY_PREMIUM_FREQUENCY = ['Monthly', 'Quarterly', 'Annually', 'Biannually'];
const POLICY_TYPE = ['Term life', 'Whole life', 'Universal life', 'Variable life', 'Long term disability',
    'Short term disability', 'Supplement disability', 'Individual long term care', 'Joint long term care',
    'Fixed annuity', 'Variable annuity', 'Indexed annuity', 'Auto', 'Homeowners', 'Commercial property'];

const generate = (config, fileHandler, entityId, insuredEntityId = '', relatedEntityId = '', insuredContactId = '', assetLiabilityId = '', serviceId = '') => {
    let endDate = faker.date.between(DATES_START_END[0], DATES_START_END[1]);
    const policyCoverage = {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "Name": `${faker.random.words(5)} - Policy Coverage`,
        "practifi__Amount__c": faker.datatype.number(50000),
        "practifi__Asset_Liability__c": assetLiabilityId, //TODO -add
        "practifi__End_Date__c": endDate.toISOString().split('T')[0],
        "practifi__Entity__c": entityId,
        "practifi__Insured__c": insuredEntityId,
        "practifi__Person_Insured__c": insuredContactId,//TODO - add
        "practifi__Policy_Number__c": faker.lorem.slug(5),
        "practifi__Premium_Amount__c": faker.datatype.number(500),
        "practifi__Premium_Frequency__c": faker.random.arrayElement(POLICY_PREMIUM_FREQUENCY),
        "practifi__Related_Entity__c": relatedEntityId,
        // "practifi__Start_Date__c": 
        "practifi__Type__c": faker.random.arrayElement(POLICY_TYPE),
        "practifi__Asset_Liability__r.Id": assetLiabilityId, //TODO -add
        "practifi__Entity__r.Id": entityId,
        "practifi__Insured__r.Id": insuredEntityId,
        "practifi__Person_Insured__r.Id": insuredContactId,//TODO - add
        "practifi__Related_Entity__r.Id": relatedEntityId
    }
    policyCoverage["practifi__Start_Date__c"] = faker.date.between('10/07/2020', endDate).toISOString().split('T')[0];
    policyCoverage["practifi__Policy__c"] = policyGenerator(config, fileHandler, policyCoverage, serviceId);
    //save the activity
    console.log('----Policy Cov---');
    fileHandler.appendToFile(POLICY_COVERAGE, formatData(POLICY_COVERAGE_FIELD_LIST, policyCoverage));

    // taskGenerator(config, fileHandler, 'Policy Coverage', insuredContactId, policyCoverage.Id)
    // eventGenerator(config, fileHandler, 'Policy Coverage', insuredContactId, policyCoverage.Id)
}

module.exports = generate;