const faker = require('faker');
const { formatData, findConfig, filterArrayData, findFromCSV } = require('../helpers/data_helper');
const { DEAL, DEAL_FIELD_LIST, DEAL_SERVICE_CATEGORY } = require('../const');

const serviceGenerator = require('./service');
const taskGenerator = require('./task');
const eventGenerator = require('./event');
const processGenerator = require('./process');
//picklists
const RATING = ['Hot', 'Warm', 'Cool'];
// const STAGE = ['Potential', 'Meeting Booked', 'Needs Analysis', 'Presentation', 'Closed Won', 'Closed Lost'];
const STAGE = ['Potential', 'Meeting Booked', 'Needs Analysis', 'Presentation']; //if deal is closed won then a service is opened
const STAGE_CLOSED_WON = 'Closed Won';

const generate = (config, fileHandler, accountEntity) => { //a deal is closed won if the accountEntity status is client
    const dealCategory = faker.random.arrayElement(fileHandler.getOpenFile(DEAL_SERVICE_CATEGORY).stream); //financial planning or investment management
    const deal = {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "Name": `${accountEntity.Name} - ${dealCategory.Name}`.substring(0, 80),
        // "practifi__Annual_Revenue__c",
        "practifi__AUM__c": faker.datatype.number(50000),
        "practifi__Entity__c": accountEntity.Id,
        "practifi__Related_Entity__c": accountEntity.Id,
        // "practifi__Service__c"
        "practifi__Service_Type__c": dealCategory.Id,
        "practifi__Stage__c": accountEntity['practifi__Client_Stage__c'] == 'Client' ? STAGE_CLOSED_WON : faker.random.arrayElement(STAGE),
        "practifi__Entity__r.Id": accountEntity.Id,
        "practifi__Related_Entity__r.Id": accountEntity.Id,
        // "practifi__Service__r.Id": ,
        "practifi__Service_Type__r.practifi__Code__c": dealCategory["practifi__Code__c"]
    }

    deal['practifi__Annual_Revenue__c'] = deal['practifi__AUM__c'] * faker.datatype.number({ min: .5, max: 1.5, precision: 2 });

    if (deal['practifi__Stage__c'] == STAGE_CLOSED_WON) { //only if the deal is closed won create a service and attach the service to 
        deal['practifi__Close_Date__c'] = accountEntity['practifi__Became_Client_On__c']
        deal['practifi__Service__c'] = serviceGenerator(config, fileHandler, accountEntity, deal, dealCategory);
        dealCategory["practifi__Service__r.Id"] = deal['practifi__Service__c'];
    }

    //save the deal
    console.log('----Deal---');
    fileHandler.appendToFile(DEAL, formatData(DEAL_FIELD_LIST, deal));

    processGenerator(config, fileHandler, accountEntity, '', deal.Id);
}

module.exports = generate;

/**
 *
 * if(HOUSEHOLD['client_stage'] == 'Prospect')
 * create deal(name="{householdname}_{investimentmanagement|financialplanning}", open, stage="potential", Entity=HOUSEHOLD.id)
 */
/**
 * if(HOUSEHOLD['client_stage'] == 'Client')
 * create deal(name="{householdname}_{investimentmanagement|financialplanning}", open, stage="closed won", Entity=HOUSEHOLD.id)
 * service(name="{householdname}_{investimentmanagement|financialplanning}", stage="draft"(default), Entity=HOUSEHOLD.id, (servicetype)from readonlyfile, client= HOUSEHOLD.id)
 */