//required libararies
const faker = require('faker');
const { createHeader, formatData, findConfig, filterArrayData, findFromCSV } = require('../helpers/data_helper');
const { ACCOUNT, ACCOUNT_FIELD_LIST, ACCOUNT_GENERATE_TYPE,
    ACCOUNT_DEFAULT_DATA_SIZE, RECORD_TYPE, RELATIONSHIP,
    RELATIONSHIP_FIELD_LIST, DEAL, SERVICE, ASSET_LIABILITY,
    ASSET_LIABILITY_FIELD_LIST, SERVICE_FIELD_LIST, DEAL_FIELD_LIST,
    DATES_START_END, HOLDING, HOLDING_FIELD_LIST,
    CONTACT, CONTACT_FIELD_LIST,
    EVENT, EVENT_FIELD_LIST, TASKS, TASK_FIELD_LIST,
    POLICY_COVERAGE, POLICY_COVERAGE_FIELD_LIST,
    POLICY, POLICY_FIELD_LIST,
    PROCESS, PROCESS_FIELD_LIST,
    CONTACT_POINT_PHONE, CONTACT_POINT_PHONE_FIELD_LIST,
    CONTACT_POINT_ADDRESS, CONTACT_POINT_ADDRESS_FIELD_LIST,
    CONTACT_POINT_EMAIL, CONTACT_POINT_EMAIL_FIELD_LIST,
    REFERENCE_DOCUMENT, REFERENCE_DOCUMENT_FIELD_LIST
} = require('../const');
const relationshipGenerator = require('./relationship');
const dealGenerator = require('./deal');
const contactGenerator = require('./contact');
const taskGenerator = require('./task');
const eventGenerator = require('./event');
const policyCoverageGenerator = require('./policyCoverage');
const processGenerator = require('./process');
const referenceDocument = require('./referenceDocument');

const TYPE_INDIVIDUAL = 'Individual';
const TYPE_HOUSEHOLD = 'Household';
const TYPE_ORGANIZATION = 'Organization';

const CLIENT_STAGE_PROSPECT = 'Prospect';
const CLIENT_STAGE_CLIENT = 'Client';

//Account Picklists
const ACCOUNT_SOURCE = ['Web', 'Phone Inquiry', 'Partner Referral', 'Purchased List', 'Other'];
const CITIZENSHIP_STATUS = ['US Citizen', 'Resident Alien', 'Non-Resident Alien'];
const CLIENT_SEGMENT = ['Platinum', 'Gold', 'Silver', 'Bronze'];
// const CLIENT_STAGE = ['Prospect', 'Lost Prospect', 'Client', 'Lost Client'];
const CLIENT_STAGE = [CLIENT_STAGE_PROSPECT, CLIENT_STAGE_CLIENT, ''];
const EMPLOYMENT_STATUS = ['Permanent Employee', 'Contractor', 'Casual', 'Director', 'Partner', 'Sole Trader', 'Homemaker', 'Retired', 'Unemployed', 'Other'];
const GENDER = ['Male', 'Female', 'Other'];
const HOUSING_STATUS = ['Own Home', 'Own Home (Mortgage)', 'Renting', 'Living With Family', 'Other'];
const INDUSTRY = ['Agriculture', 'Apparel', 'Banking', 'Biotechnology', 'Chemicals', 'Communications', 'Construction', 'Consulting', 'Education', 'Electronics', 'Energy', 'Engineering', 'Entertainment', 'Environmental', 'Finance', 'Food & Beverage', 'Government', 'Healthcare', 'Hospitality', 'Insurance', 'Machinery', 'Manufacturing', 'Media', 'Not For Profit', 'Recreation'];
const INFLUENCER_SEGMENT = ['Standard', 'Important', 'Critical'];
const LEAD_SOURCE = ['Web', 'Phone Inquiry', 'Partner Referral', 'Purchased List', 'Other'];
const LOSS_REASON = ['Client', 'Custodian', 'Letter'];
const MARITAL_STATUS = ['Single', 'Married', 'De Facto', 'Separated', 'Divorced', 'Widowed'];
const ORGANIZATION_TYPE = ['Company', 'Trust', 'Community Partner', 'Professional Partner', 'Other'];
const PARTNER_TYPE = ['Attorney', 'CPA', 'Tax Expert', 'Estate Planner', 'Insurance Agent', 'Other'];
const PREFERRED_EMAIL = ['Household Email', 'Organization Email', 'Primary Contact Email', 'Individual Email'];
const PREFERRED_PHONE = ['Home Phone', 'Mobile Phone', 'Other Phone', 'Household Phone', 'Organization Phone', 'Primary Contact Home', 'Primary Contact Mobile', 'Primary Contact Other'];
const RATING = ['Hot', 'Warm', 'Cold'];
const REASON_FOR_LOSS = ['Undisclosed', 'Competition', 'Deceased', 'Fees', 'Fees', 'Move', 'Self-manage', 'Service'];
const SOURCE = ['Referral', 'Team Member Referral', 'Client Referral', 'Website Enquiry', 'Email Campaign'];
const STATE_OF_RESIDENCE = ['AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY']
const TRUST_TYPE = ['Revocable', 'Irrevocable'];
const TRUSTEE_SIZE = ['Corporate', 'Individual'];
// const TYPE__C = [TYPE_INDIVIDUAL, TYPE_HOUSEHOLD, TYPE_ORGANIZATION];
const TYPE__C = [TYPE_INDIVIDUAL, TYPE_HOUSEHOLD, TYPE_HOUSEHOLD, TYPE_HOUSEHOLD]; //HH:Individual ratio 3:1
const TYPE = ['Prospect', 'Customer - Direct', 'Customer - Channel', 'Channel Partner / Reseller', 'Installation Partner', 'Technology Partner', 'Other'];
const SALUTATION = ['Mr.', 'Mrs.']
const DAYS_SINCE_LAST_CONTACT_RANGE = ['1 - 15', '16 - 30', '31 - 60', '61 - 90', '91+'];

const RECORD_TYPE_LIST = {
    "ACCOUNT": "Individual;Account",
    "INDIVIDUAL_MEMBER": "Individual_Member;Account",
    "HOUSEHOLD": "Household;Account",
    "HOUSEHOLD_CLIENT": "Household_Client;Account",
    "HOUSEHOLD_PROSPECT": "Household_Prospect;Account",
    "INDIVIDUAL_PROSPECT": "Individual_Prospect;Account",
    "INDIVIDUAL_CLIENT": "Individual_Client;Account"
}

const openWriteableFiles = (fileHandler) => {
    fileHandler.refreshFolderName();
    fileHandler.openFile(ACCOUNT, false, createHeader(ACCOUNT_FIELD_LIST));
    fileHandler.openFile(CONTACT, false, createHeader(CONTACT_FIELD_LIST));
    fileHandler.openFile(RELATIONSHIP, false, createHeader(RELATIONSHIP_FIELD_LIST));
    fileHandler.openFile(DEAL, false, createHeader(DEAL_FIELD_LIST));
    fileHandler.openFile(SERVICE, false, createHeader(SERVICE_FIELD_LIST));
    fileHandler.openFile(ASSET_LIABILITY, false, createHeader(ASSET_LIABILITY_FIELD_LIST));
    fileHandler.openFile(HOLDING, false, createHeader(HOLDING_FIELD_LIST));
    fileHandler.openFile(EVENT, false, createHeader(EVENT_FIELD_LIST));
    fileHandler.openFile(TASKS, false, createHeader(TASK_FIELD_LIST));
    fileHandler.openFile(POLICY_COVERAGE, false, createHeader(POLICY_COVERAGE_FIELD_LIST));
    fileHandler.openFile(POLICY, false, createHeader(POLICY_FIELD_LIST));
    fileHandler.openFile(PROCESS, false, createHeader(PROCESS_FIELD_LIST));
    fileHandler.openFile(CONTACT_POINT_PHONE, false, createHeader(CONTACT_POINT_PHONE_FIELD_LIST));
    fileHandler.openFile(CONTACT_POINT_ADDRESS, false, createHeader(CONTACT_POINT_ADDRESS_FIELD_LIST));
    fileHandler.openFile(CONTACT_POINT_EMAIL, false, createHeader(CONTACT_POINT_EMAIL_FIELD_LIST));
    fileHandler.openFile(REFERENCE_DOCUMENT, false, createHeader(REFERENCE_DOCUMENT_FIELD_LIST));
}

const generate = (config, fileHandler) => {
    //Extract config data
    const CONFIG_NUMBER_OF_DATA_GEN = findConfig(config, 'config.account.data_count'.split('.'));
    const CONFIG_TYPE__C = filterArrayData(findConfig(config, 'config.account.picklist_options.TYPE_C'.split('.')), TYPE__C, true);

    //create the files
    openWriteableFiles(fileHandler);

    //loop and generate the type of data 
    for (let i = 1; i <= (CONFIG_NUMBER_OF_DATA_GEN || ACCOUNT_DEFAULT_DATA_SIZE); i++) {
        console.log(`--------------------${i}-----------------------`);
        Array.isArray(CONFIG_TYPE__C) ?
            generatorHandler(CONFIG_TYPE__C.length > 1 ? faker.random.arrayElement(CONFIG_TYPE__C) : CONFIG_TYPE__C[0], config, fileHandler) :
            generatorHandler(faker.random.arrayElement(TYPE__C), config, fileHandler);
    }

}

const generatorHandler = (generateType, config, fileHandler) => {
    switch (generateType) {
        case TYPE_INDIVIDUAL: //create an individual and save it
            console.log('Generate Individual');
            let startDate = DATES_START_END[0];
            const individualType = faker.random.arrayElement([RECORD_TYPE_LIST["INDIVIDUAL_CLIENT"], RECORD_TYPE_LIST["INDIVIDUAL_PROSPECT"]]); //may be i can have individual
            const individualTypeId = findFromCSV(fileHandler.getOpenFile(RECORD_TYPE).stream, '$$DeveloperName$SobjectType', individualType);
            const individualAccount = generateAccount(config, fileHandler, individualTypeId); //check thiis TODO
            individualAccount["practifi__Type__c"] = TYPE_INDIVIDUAL;
            startDate = faker.date.between(startDate, DATES_START_END[1]);
            individualAccount["practifi__Became_Prospect_On__c"] = startDate.toISOString().split('T')[0];
            individualAccount["practifi__Definitions__c"] = individualAccount["Prospect"];

            if (individualType == RECORD_TYPE_LIST["INDIVIDUAL_CLIENT"]) {
                startDate = faker.date.between(startDate, DATES_START_END[1]);
                individualAccount["practifi__Client_Stage_Entry_Date__c"] = startDate.toISOString().split('T')[0];
                individualAccount["practifi__Became_Client_On__c"] = startDate.toISOString().split('T')[0];
                individualAccount["practifi__Client_Date__c"] = individualAccount["practifi__Became_Client_On__c"]
                individualAccount["practifi__Definitions__c"] = individualAccount["Client"];
                individualAccount["practifi__Client_Segment__c"] = faker.random.arrayElement(CLIENT_SEGMENT);

                //Create a policy + policy coverage
                policyCoverageGenerator(config, fileHandler, individualAccount.Id, individualAccount.Id);
                processGenerator(config, fileHandler, individualAccount);
            }
            console.log('----Account(Individual)---');
            fileHandler.appendToFile(ACCOUNT, formatData(ACCOUNT_FIELD_LIST, individualAccount));
            const taskToGenerate = faker.datatype.number(2);
            for (let i = 0; i <= taskToGenerate; i++) {
                taskGenerator(config, fileHandler, 'Person Account', '', individualAccount.Id);
                eventGenerator(config, fileHandler, 'Person Account', '', individualAccount.Id);
                referenceDocument(config, fileHandler, individualAccount);
            }

            break;

        case TYPE_HOUSEHOLD:
            console.log('Generate household');
            //create a household
            const householdType = faker.random.arrayElement([RECORD_TYPE_LIST["HOUSEHOLD"], RECORD_TYPE_LIST["HOUSEHOLD_CLIENT"], RECORD_TYPE_LIST["HOUSEHOLD_PROSPECT"]]);
            const householdTypeId = findFromCSV(fileHandler.getOpenFile(RECORD_TYPE).stream, '$$DeveloperName$SobjectType', householdType);

            const household = generateHousehold(config, householdTypeId, householdType);

            const primaryContact = contactGenerator(config, fileHandler, null, household);
            const partnerContact = faker.datatype.boolean() ? contactGenerator(config, fileHandler, null, household) : null;

            const dependants = [];
            if (faker.datatype.boolean()) {
                const dependantsCount = faker.datatype.number(2);
                for (let i = 0; i <= dependantsCount; i++) {
                    const dependant = contactGenerator(config, fileHandler, null, household);
                    dependants.push(dependant);
                }
            }

            household.Name = `${primaryContact.LastName}, ${primaryContact.FirstName}`;

            household["practifi__Primary_Member__c"] = primaryContact["practifi__Primary_Member__c"];
            household["practifi__Primary_Member__r.Id"] = primaryContact["practifi__Primary_Member__r.Id"];

            if (partnerContact) {
                household.Name += `${dependants.length ? ',' : ' &'} ${partnerContact.LastName}, ${partnerContact.FirstName}`;
                household["practifi__Spouse__c"] = partnerContact.Id;
                household["practifi__Spouse__r.Id"] = partnerContact.Id;
            }

            if (dependants.length) {
                household.Name = dependants.reduce((str, dependant, idx) => str + (idx == dependants.length - 1 ? ' & ' : ', ') + dependant.FirstName, household.Name)
                for (const dependant of dependants) {
                    //add to the relationship TODO
                    relationshipGenerator(config, fileHandler, dependant, household, 'RTDEPEND');
                }
            }

            //add to relationship
            relationshipGenerator(config, fileHandler, primaryContact, household, 'RTPRICONT');
            if (partnerContact) relationshipGenerator(config, fileHandler, partnerContact, household, 'RTPARTNER');


            //add houshold to account
            console.log('----Account(HH)---');
            fileHandler.appendToFile(ACCOUNT, formatData(ACCOUNT_FIELD_LIST, household));

            //for more conditions
            if (household["practifi__Client_Stage__c"]) { //only if the client is stage is not blank
                //it can be either prospecting or a client get a deal
                dealGenerator(config, fileHandler, household); //TODO
            }

            const taskEventGenerateHH = faker.datatype.number(3);
            for (let i = 0; i <= taskEventGenerateHH; i++) {
                taskGenerator(config, fileHandler, 'Household', primaryContact.Id, household.Id); //primary
                eventGenerator(config, fileHandler, 'Household', primaryContact.Id, household.Id); //primary
                if (partnerContact) {
                    taskGenerator(config, fileHandler, 'Household', partnerContact.Id, household.Id); //partner
                    eventGenerator(config, fileHandler, 'Household', partnerContact.Id, household.Id); //partner
                }
                for (const dependant of dependants) {
                    taskGenerator(config, fileHandler, 'Household', dependant.Id, household.Id);;
                    eventGenerator(config, fileHandler, 'Household', dependant.Id, household.Id);;
                }
            }

            if (RECORD_TYPE_LIST["HOUSEHOLD_CLIENT"] == householdType) { //generate policies
                policyCoverageGenerator(config, fileHandler, household.Id, '', household.Id, primaryContact.Id); //primary
                processGenerator(config, fileHandler, household, primaryContact.Id);
                if (partnerContact) {
                    policyCoverageGenerator(config, fileHandler, household.Id, '', household.Id, partnerContact.Id);
                    processGenerator(config, fileHandler, household, '', '', partnerContact.Id);
                } //partner
                for (const dependant of dependants) {
                    policyCoverageGenerator(config, fileHandler, household.Id, '', household.Id, dependant.Id);
                    processGenerator(config, fileHandler, household, '', '', dependant.Id);
                }
            }

            for (let i = 0; i < 2; i++) {
                referenceDocument(config, fileHandler, household, primaryContact.Id);
                if (partnerContact) referenceDocument(config, fileHandler, household, partnerContact.Id); //partner
                for (const dependant of dependants) referenceDocument(config, fileHandler, household, dependant.Id);
            }
            break;
        default:
            return null
    }
}

const generateHousehold = (config, recordType, householdType) => {

    const household = {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        // "Name": ,
        "practifi__Type__c": TYPE_HOUSEHOLD,
        // "practifi__Client_Stage__c": ,
        "RecordTypeId": recordType['Id'],
        "IsPersonAccount": 'false',
        // "practifi__Became_Prospect_On__c": ,
        // "practifi__Became_Client_On__c": ,
        // "practifi__Client_Stage_Entry_Date__c": ,
        // "practifi__Client_Date__c": ,
        // "practifi__AUM__c": ,
        // "practifi__Primary_Contact__c": ,
        "practifi__Won_Client__c": 0,
        // "practifi__Primary_Contact__r.Id",
        // "practifi__Partner__r.Id",
        "practifi__Days_Since_Last_Contact_Range__c": faker.random.arrayElement(DAYS_SINCE_LAST_CONTACT_RANGE),
        "RecordType.$$DeveloperName$SobjectType": recordType['$$DeveloperName$SobjectType']
    }
    let startDate = DATES_START_END[0];
    switch (householdType) {
        case RECORD_TYPE_LIST["HOUSEHOLD_CLIENT"]:
            household["practifi__Client_Stage__c"] = CLIENT_STAGE_CLIENT;
            startDate = faker.date.between(startDate, DATES_START_END[1]);
            household["practifi__Became_Prospect_On__c"] = startDate.toISOString().split('T')[0];
            startDate = faker.date.between(startDate, DATES_START_END[1]);
            household["practifi__Client_Stage_Entry_Date__c"] = startDate.toISOString().split('T')[0];
            household["practifi__Became_Client_On__c"] = startDate.toISOString().split('T')[0];
            household["practifi__Client_Date__c"] = household["practifi__Became_Client_On__c"];
            household["practifi__AUM__c"] = faker.datatype.number(50000);
            household["practifi__Won_Client__c"] = 1;
            household["practifi__Client_Segment__c"] = faker.random.arrayElement(CLIENT_SEGMENT);
            break;
        case RECORD_TYPE_LIST["HOUSEHOLD_PROSPECT"]:
            household["practifi__Client_Stage__c"] = CLIENT_STAGE_PROSPECT;
            startDate = faker.date.between(startDate, DATES_START_END[1]);
            household["practifi__Became_Prospect_On__c"] = startDate.toISOString().split('T')[0];
            household["practifi__Potential_AUM__c"] = faker.datatype.number(50000);
            household["practifi__Potential_Revenue__c"] = faker.datatype.number(50000);
            break;
        default:
            household["practifi__Client_Stage__c"] = "";
            break;
    }

    household["practifi__Definitions__c"] = household["practifi__Client_Stage__c"];
    household["practifi__Last_Contact_Date__c"] = getLastContactDateFromRange(household["practifi__Days_Since_Last_Contact_Range__c"]);

    return household;
}

const generateAccount = (config, fileHandler, recordType) => {
    /**
     * Generate an account and send it back 
     * Gender, salutation, firstname, lastname, email, phonenumber, 
     */
    const account = {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "FirstName": faker.name.firstName(),
        "LastName": faker.name.lastName(),
        "Salutation": faker.random.arrayElement(SALUTATION),
        // "practifi__Type__c": TYPE_INDIVIDUAL,
        // "practifi__Client_Stage_Entry_Date__c": ,
        // "practifi__Client_Date__c": ,
        // "practifi__Definitions__c": ,
        // "practifi__Primary_Entity_Stage__c": , 
        // "practifi__Primary_Entity__c": ,
        "practifi__Won_Client__c": 0,
        "RecordTypeId": recordType['Id'],
        "IsPersonAccount": 'true',
        "practifi__Days_Since_Last_Contact_Range__c": faker.random.arrayElement(DAYS_SINCE_LAST_CONTACT_RANGE),
        "RecordType.$$DeveloperName$SobjectType": recordType['$$DeveloperName$SobjectType']
    }
    account["practifi__Last_Contact_Date__c"] = getLastContactDateFromRange(account["practifi__Days_Since_Last_Contact_Range__c"]);
    account.Name = `${account.FirstName} ${account.LastName}`;
    contactGenerator(config, fileHandler, account);
    return account;
}

const getLastContactDateFromRange = (dateRange) => {
    let today = new Date();
    if (dateRange != '91+') {
        return new Date(today.setDate(today.getDate() - faker.datatype.number({ min: parseInt(dateRange.split(' - ')[0]), max: parseInt(dateRange.split(' - ')[0]) }))).toISOString().split('T')[0];
    }
    return new Date(today.setDate(today.getDate() - faker.datatype.number({ min: 91, max: 360 }))).toISOString().split('T')[0];
}

module.exports = generate;

/**
 * Date Formats
 * practifi__Became_Prospect_On__c => "2020-07-20"
 * practifi__Client_Stage_Entry_Date__c => "2020-07-31"
 * practifi__Became_Client_On__c => "2020-09-28"
 * practifi__Client_Date__c => "2020-08-13"
 *
 */