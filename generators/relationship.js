const faker = require('faker');
const { formatData, findConfig, filterArrayData, findFromCSV } = require('../helpers/data_helper');
const { RELATIONSHIP, RELATIONSHIP_TYPE, RELATIONSHIP_FIELD_LIST } = require('../const');

const generate = (config, fileHandler, fromContact, toEntity, relationshipCode) => {
    //generate and store the data
    const relationship = findFromCSV(fileHandler.getOpenFile(RELATIONSHIP_TYPE).stream, 'practifi__Code__c', relationshipCode);
    if (!relationship) {
        console.log(`Predefined relationship not found for: `, relationshipCode)
        return;
    }
    console.log('----Relationship---');
    fileHandler.appendToFile(RELATIONSHIP, formatData(RELATIONSHIP_FIELD_LIST, {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "Name": `${fromContact.Name} - ${toEntity.Name}`.substring(0, 80),
        // "practifi__Description__c",
        "practifi__Relationship_Type__c": relationship.Id,
        "practifi__From_Contact__c": fromContact.Id,
        "practifi__To_Entity__c": toEntity.Id,
        "practifi__Relationship_Type__r.practifi__Code__c": relationshipCode,
        "practifi__From_Contact__r.Id": fromContact.Id,
        "practifi__To_Entity__r.Id": toEntity.Id
    }));
}

module.exports = generate;

// practifi__Code__c
// RTPRICONT
// RTPARTNER
// RTDEPENDs