const faker = require('faker');
const { formatData, findConfig, filterArrayData, findFromCSV } = require('../helpers/data_helper');
const { REFERENCE_DOCUMENT, REFERENCE_DOCUMENT_FIELD_LIST, DATES_START_END } = require('../const');

//picklist
const referenceDocumentType = ['Driver’s License', 'State ID', 'Passport', 'Other'];

const generate = (config, fileHandler, entity, contactId = '') => {
    let startDate = faker.date.between(DATES_START_END[0], DATES_START_END[1]);
    let endDate = faker.date.between(DATES_START_END[0], DATES_START_END[1]);
    const referenceDocument = {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "Name": `${entity.Name} ${faker.random.word()} - Reference Doc`.substring(0, 80),
        "practifi__Description__c": faker.random.words(15),
        "practifi__Document_Number__c": faker.random.words(10),
        "practifi__Document_URL__c": faker.internet.url(),
        "practifi__Entity__c": entity.Id,
        "practifi__Expiration_Date__c": endDate.toISOString().split('T')[0],
        "practifi__Issue_Date__c": startDate.toISOString().split('T')[0],
        "practifi__Issuing_Authority__c": faker.company.companyName(),
        "practifi__Person__c": contactId,
        "practifi__Points__c": faker.datatype.number({ min: 2, max: 10, precision: 2 }),
        "practifi__Related_Entity__c": entity.Id,
        "practifi__Type__c": faker.random.arrayElement(referenceDocumentType),
        "practifi__Entity__r.Id": entity.Id,
        "practifi__Person__r.Id": contactId,
        "practifi__Related_Entity__r.Id": entity.Id

    }

    //save the activity
    console.log('----Referecne Doc---');
    fileHandler.appendToFile(REFERENCE_DOCUMENT, formatData(REFERENCE_DOCUMENT_FIELD_LIST, referenceDocument));
}
module.exports = generate;

/**
practifi__Description__c=> Long Text Area(32768)
practifi__Document_Number__c=> Text(255)
practifi__Document_URL__c=> URL(255)
practifi__Entity__c=> Lookup(Entity)
practifi__Expiration_Date__c=> Date
practifi__Issue_Date__c=> Date
practifi__Issuing_Authority__c=> Text(255)
practifi__Person__c=> Lookup(Contact)
practifi__Points__c=> Number(16, 2)
Name=> Text(80)
practifi__Related_Entity__c=> Lookup(Entity)
practifi__Type__c=> Picklist
 *
 */