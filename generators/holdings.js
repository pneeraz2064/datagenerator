const faker = require('faker');
const { formatData, findConfig, filterArrayData, findFromCSV } = require('../helpers/data_helper');
const { HOLDING, HOLDING_FIELD_LIST, DATES_START_END } = require('../const');

//picklists
const HOLDING_STAGE = ['Owned', 'Disposed'];

const generate = (config, fileHandler, assetLiability) => {
    const holding = {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "Name": faker.company.companyName(),
        "practifi__Revenue__c": assetLiability["practifi__Value__c"],
        "practifi__As_At__c": faker.date.between(DATES_START_END[0], DATES_START_END[1]).toISOString().split('T')[0],
        "practifi__Asset_Liability__c": assetLiability.Id,
        "practifi__Balance__c": assetLiability["practifi__Value__c"],
        "practifi__Class_Name__c": faker.lorem.word(),
        "practifi__Price__c": assetLiability["practifi__Value__c"],
        "practifi__Product_Code__c": faker.lorem.word(),
        "practifi__Stage__c": faker.random.arrayElement(HOLDING_STAGE),
        "practifi__Unitised__c": faker.datatype.boolean(),
        "practifi__Asset_Liability__r.Id": assetLiability.Id
    }

    console.log('----Holding---');
    fileHandler.appendToFile(HOLDING, formatData(HOLDING_FIELD_LIST, holding));
}

module.exports = generate;

/**
 * Date Format
 * practifi__As_At__c => "2020-07-15"
 */