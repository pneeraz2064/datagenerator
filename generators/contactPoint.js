//imports
const faker = require('faker');
const { CONTACT_POINT_PHONE, CONTACT_POINT_PHONE_FIELD_LIST,
    CONTACT_POINT_ADDRESS, CONTACT_POINT_ADDRESS_FIELD_LIST,
    CONTACT_POINT_EMAIL, CONTACT_POINT_EMAIL_FIELD_LIST } = require('../const');
const { formatData } = require('../helpers/data_helper');

const PHONE_MAP_TO_RECORD = ['Home', 'Mobile', 'Other'];
const ADDRESS_MAP_TO_RECORD = ['Mailing', 'Other'];
const EMAIL_MAP_TO_RECORD = ['Email', 'Alternate Email'];
const USAGE_TYPE = ['Home', 'Work', 'Other', 'Personal'];
const ADDRESS_TYPE = ['Billing', 'Shipping'];
//picklists
const contactPointPhoneGenerator = (contact) => {
    return {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "practifi__Contact__c": contact.Id,
        "practifi__Description__c": faker.lorem.words(10),
        "ExtensionNumber": faker.datatype.number(100),
        "FormattedInternationalPhoneNumber": faker.phone.phoneNumber(),
        "IsBusinessPhone": faker.datatype.boolean(),
        "IsPersonalPhone": faker.datatype.boolean(),
        "IsPrimary": faker.datatype.boolean(),
        "IsSmsCapable": faker.datatype.boolean(),
        "practifi__Map_to_Record__c": faker.random.arrayElement(PHONE_MAP_TO_RECORD),
        // "PhoneType": ,
        "PreferenceRank": faker.datatype.float({ min: 0, max: 50, precision: 2 }),
        "TelephoneNumber": faker.phone.phoneNumber(),
        "UsageType": faker.random.arrayElement(USAGE_TYPE),
        "practifi__Contact__r.Id": contact.Id
    }

}


const contactPointEmailGenerator = (contact) => {
    return {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "practifi__Contact__c": contact.Id,
        "EmailAddress": faker.internet.email(),
        "EmailDomain": faker.internet.domainName(),
        "IsPrimary": faker.datatype.boolean(),
        "practifi__Map_to_Record__c": faker.random.arrayElement(EMAIL_MAP_TO_RECORD),
        "UsageType": faker.random.arrayElement(USAGE_TYPE),
        "practifi__Contact__r.Id": contact.Id
    }
}


const contactPointAddressGenerator = (contact, contactPointPhone) => {
    return {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "Name": contact.Name,
        "Street": faker.address.streetAddress(),
        "City": faker.address.city(),
        "State": faker.address.state(),
        "Country": faker.address.country(),
        "AddressType": faker.random.arrayElement(ADDRESS_TYPE),
        "practifi__Contact__c": contact.Id,
        "ContactPointPhoneId": contactPointPhone.Id,
        "practifi__Description__c": faker.lorem.words(10),
        "IsDefault": faker.datatype.boolean(),
        "IsPrimary": faker.datatype.boolean(),
        "practifi__Map_to_Record__c": faker.random.arrayElement(ADDRESS_MAP_TO_RECORD),
        "PreferenceRank": faker.datatype.float({ min: 0, max: 50, precision: 2 }),
        "TelephoneNumber": faker.phone.phoneNumber(),
        "UsageType": faker.random.arrayElement(USAGE_TYPE),
        "practifi__Contact__r.Id": contact.Id,
        "ContactPointPhone.Id": contactPointPhone.Id
    }
}

const generate = (config, fileHandler, contact) => {
    const contactPointPhone = contactPointPhoneGenerator(contact);
    const contactPointAddress = contactPointAddressGenerator(contact, contactPointPhone);
    const contactPointEmail = contactPointEmailGenerator(contact);
    console.log('----Contact Point (Phone)---');
    fileHandler.appendToFile(CONTACT_POINT_PHONE, formatData(CONTACT_POINT_PHONE_FIELD_LIST, contactPointPhone));
    console.log('----Contact Point (Address)---');
    fileHandler.appendToFile(CONTACT_POINT_ADDRESS, formatData(CONTACT_POINT_ADDRESS_FIELD_LIST, contactPointAddress));
    console.log('----Contact Point (Email)---');
    fileHandler.appendToFile(CONTACT_POINT_EMAIL, formatData(CONTACT_POINT_EMAIL_FIELD_LIST, contactPointEmail));
    //generate and save the contact point email, address and phone
}

module.exports = generate;