const faker = require('faker');
const { formatData, findConfig, filterArrayData, findFromCSV } = require('../helpers/data_helper');
const { PROCESS, PROCESS_FIELD_LIST, DATES_START_END, PROCESS_STAGE, PROCESS_TYPE, PROCESS_TASK, } = require('../const');
const taskGenerator = require('./task');
const eventGenerator = require('./event');

const generate = (config, fileHandler, entity, assetLiabilityId = '', dealId = '', contactId = '', serviceId = '') => {
    let startDate = faker.date.between(DATES_START_END[0], DATES_START_END[1]);
    let completedDate = faker.date.between(DATES_START_END[0], DATES_START_END[1]);
    let processType = faker.random.arrayElement(fileHandler.getOpenFile(PROCESS_TYPE).stream);
    let processStage = faker.random.arrayElement(fileHandler.getOpenFile(PROCESS_STAGE).stream);
    const process = {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "Name": `${entity.Name} ${faker.random.word()} - Process`.substring(0, 80),
        "practifi__Asset_Liability__c": assetLiabilityId,
        "practifi__Completed_Date__c": completedDate.toISOString().split('T')[0],
        "practifi__Completed_Datetime__c": completedDate.toISOString(),
        "practifi__Deal__c": dealId,
        "practifi__Description__c": faker.lorem.words(10),
        "practifi__Due_Date__c": completedDate.toISOString().split('T')[0],
        "practifi__Entity__c": entity.Id,
        "practifi__Person__c": contactId,
        "practifi__Process_Type__c": processType.Id,
        "practifi__Related_Client__c": entity.Id,
        "practifi__Service__c": serviceId,
        "practifi__Stage__c": processStage.Id,
        "practifi__Stage_Entry_Date__c": startDate.toISOString().split('T')[0],
        "practifi__Started_Date__c": startDate.toISOString().split('T')[0],
        "practifi__Asset_Liability__r.Id": assetLiabilityId,
        "practifi__Deal__r.Id": dealId,
        "practifi__Entity__r.Id": entity.Id,
        "practifi__Person__r.Id": contactId,
        "practifi__Related_Client__r.Id": entity.Id,
        "practifi__Service__r.Id": serviceId,
    }

    //save the activity
    console.log('----Process---');
    fileHandler.appendToFile(PROCESS, formatData(PROCESS_FIELD_LIST, process));
    //create task
    // taskGenerator(config, fileHandler, 'Process', '', process.Id);
    // eventGenerator(config, fileHandler, 'Process', '', process.Id);
}
module.exports = generate;

/**
 * practifi__Asset_Liability__c => Lookup(Asset/Liability)
practifi__Completed_Date__c => Date
practifi__Completed_Datetime__c => Date/Time
practifi__Deal__c => Lookup(Deal)
practifi__Description__c => Text Area(255)
practifi__Due_Date__c => Date
practifi__Entity__c => Lookup(Account)
practifi__Financial_Product__c => Lookup(Financial Product)
practifi__Person__c => Lookup(Contact)
Name => Text(80)
practifi__Process_Type__c => Lookup(Process Type) ==
practifi__Related_Client__c => Lookup(Account)
practifi__Service__c => Lookup(Service)
practifi__Set_As_Created_Date__c => Date/Time
practifi__Stage__c => Lookup(Process Stage) ==
practifi__Stage_Entry_Date__c => Date
practifi__Started_Date__c => Date

 */