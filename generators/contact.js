const faker = require('faker');
const { formatData, findConfig, filterArrayData, findFromCSV } = require('../helpers/data_helper');
const { CONTACT, CONTACT_FIELD_LIST, DATES_START_END } = require('../const');
const generateContactPoint = require('./contactPoint');

//picklists
const CONTACT_CITIZENSHIP_STATUS = ['US Citizen', 'Resident Alien', 'Non-Resident Alien'];
const CONTACT_EMPLOYMENT_STATUS = ['Permanent Employee', 'Contractor', 'Casual', 'Director', 'Partner', 'Sole Trader', 'Homemaker', 'Retired', 'Unemployed', 'Other'];
const CONTACT_GENDER = ['Male', 'Female', 'Other'];
const CONTACT_LEAD_SOURCE = ['Web', 'Phone Inquiry', 'Partner Referral', 'Purchased List', 'Other'];
const CONTACT_MARITAL_STATUS = ['Single', 'Married', 'De Facto', 'Separated', 'Divorced', 'Widowed'];
const CONTACT_PREFERRED_PHONE = ['Home Phone', 'Mobile Phone', 'Other Phone'];
const SALUTATION = ['Mr.', 'Mrs.'];

const generate = (config, fileHandler, account = null, household = null) => {

    if (account) {
        const contact = {
            "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
            "FirstName": account["FirstName"],
            "LastName": account["LastName"],
            "Salutation": account["Salutation"],
            "Name": account["Name"],
            "AccountId": account.Id,
            "IsPersonAccount": account["IsPersonAccount"],
            "practifi__Alternate_Email__c": faker.internet.email(),
            "AssistantName": `${faker.name.firstName()} ${faker.name.lastName()}`,
            "AssistantPhone": faker.phone.phoneNumber(),
            "practifi__Birth_Name__c": account.Name,
            // "practifi__Birth_Place__c": faker.address.streetAddress(true), // Does not exists anymore
            "Birthdate": faker.date.between(DATES_START_END[0], DATES_START_END[1]).toISOString().split('T')[0],
            "practifi__Citizenship_Status__c": faker.random.arrayElement(CONTACT_CITIZENSHIP_STATUS),
            "practifi__Country_Of_Citizenship__c": faker.address.country(),
            "practifi__Country_of_Origin__c": faker.address.country(),
            "Email": faker.internet.email(),
            "practifi__Employer__c": `${faker.name.firstName()} ${faker.name.lastName()}`,
            "practifi__Gender__c": faker.random.arrayElement(CONTACT_GENDER),
            // "IndividualId": ,
            "LeadSource": faker.random.arrayElement(CONTACT_LEAD_SOURCE),
            "practifi__Marital_Status__c": faker.random.arrayElement(CONTACT_MARITAL_STATUS),
            "MobilePhone": faker.phone.phoneNumber(),
            "practifi__Preferred_Phone__c": faker.random.arrayElement(CONTACT_PREFERRED_PHONE),
            // "practifi__Primary_Entity__c": ,
            // "Title": ,
            "Account.Id": account.Id,
            // "Individual.Id": ,
            // "practifi__Primary_Entity__r.Id": ,
        }
        console.log('----Contact(Account)---');
        fileHandler.appendToFile(CONTACT, formatData(CONTACT_FIELD_LIST, contact));
        return;
    }

    const contact = {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "FirstName": faker.name.firstName(),
        "LastName": faker.name.lastName(),
        "Salutation": faker.random.arrayElement(SALUTATION),
        "AccountId": household.Id,
        "IsPersonAccount": household["IsPersonAccount"],
        "practifi__Alternate_Email__c": faker.internet.email(),
        "AssistantName": `${faker.name.firstName()} ${faker.name.lastName()}`,
        "AssistantPhone": faker.phone.phoneNumber(),
        // "practifi__Birth_Place__c": faker.address.streetAddress(true),
        "Birthdate": faker.date.between(DATES_START_END[0], DATES_START_END[1]).toISOString().split('T')[0],
        "practifi__Citizenship_Status__c": faker.random.arrayElement(CONTACT_CITIZENSHIP_STATUS),
        "practifi__Country_Of_Citizenship__c": faker.address.country(),
        "practifi__Country_of_Origin__c": faker.address.country(),
        "Email": faker.internet.email(),
        "practifi__Employer__c": `${faker.name.firstName()} ${faker.name.lastName()}`,
        "practifi__Gender__c": faker.random.arrayElement(CONTACT_GENDER),
        // "IndividualId": ,
        "LeadSource": faker.random.arrayElement(CONTACT_LEAD_SOURCE),
        "practifi__Marital_Status__c": faker.random.arrayElement(CONTACT_MARITAL_STATUS),
        "MobilePhone": faker.phone.phoneNumber(),
        "practifi__Preferred_Phone__c": faker.random.arrayElement(CONTACT_PREFERRED_PHONE),
        "practifi__Primary_Entity__c": household.Id,
        // "Title": ,
        "Account.Id": household.Id,
        // "Individual.Id": ,
        "practifi__Primary_Entity__r.Id": household.Id
    }
    contact["Name"] = `${contact.FirstName} ${contact.LastName}`;
    contact["practifi__Birth_Name__c"] = contact["Name"];

    console.log('----Contact---');
    fileHandler.appendToFile(CONTACT, formatData(CONTACT_FIELD_LIST, contact));

    const numberOfContactPoints = faker.datatype.number(2);
    for (let i = 0; i <= numberOfContactPoints; i++) generateContactPoint(config, fileHandler, contact);
    return contact;
}

module.exports = generate;

/**
 * Date formats
 * Birthdate => "1985-08-13"
 *
 *
 */