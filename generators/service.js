const faker = require('faker');
const { formatData, findConfig, filterArrayData, findFromCSV } = require('../helpers/data_helper');
const { SERVICE, SERVICE_FIELD_LIST, RECORD_TYPE } = require('../const');

const assetLiabilityGenerator = require('./assetLiability');
const processGenerator = require('./process');
//picklists
// const STAGE = ['Draft', 'Active', 'Terminated', 'Completed'];
const STAGE = ['Draft', 'Active', 'Terminated'];
const SERVICE_RECORD_TYPE = 'Service;practifi__Service__c';


const generate = (config, fileHandler, entity, deal, dealCategory) => { //a deal is closed won if the entity status is client
    //todo for record type id
    const recordType = findFromCSV(fileHandler.getOpenFile(RECORD_TYPE).stream, '$$DeveloperName$SobjectType', SERVICE_RECORD_TYPE);
    const service = {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "Name": deal.Name,
        "practifi__Annual_Revenue__c": deal["practifi__Annual_Revenue__c"],
        "practifi__AUM__c": deal["practifi__AUM__c"],
        "practifi__Client__c": entity.Id,
        "RecordTypeId": recordType.Id || '',
        "practifi__Related_Entity__c": entity.Id,
        "practifi__Service_Type__c": dealCategory.Id,
        "practifi__Stage__c": faker.random.arrayElement(STAGE),
        "practifi__Client__r.Id": entity.Id,
        "practifi__Related_Entity__r.Id": entity.Id,
        "practifi__Service_Type__r.practifi__Code__c": dealCategory['practifi__Code__c'],
        "RecordType.$$DeveloperName$SobjectType": recordType['$$DeveloperName$SobjectType'] || ''
    }

    //save the service
    console.log('----Service---');
    fileHandler.appendToFile(SERVICE, formatData(SERVICE_FIELD_LIST, service));

    //TODO Create a asset liabilty
    const assetLiabilityCount = faker.datatype.number(8);
    for (let i = 0; i <= assetLiabilityCount; i++) {
        //create asset liability
        const assetLiability = assetLiabilityGenerator(config, fileHandler, entity, service, faker.datatype.boolean());
        processGenerator(config, fileHandler, entity, assetLiability.Id, deal.Id, '', service.Id);

    }
    return service['Id'];
}

module.exports = generate;