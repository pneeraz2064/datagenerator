const faker = require('faker');
const { formatData, findConfig, filterArrayData, findFromCSV } = require('../helpers/data_helper');
const { POLICY, POLICY_FIELD_LIST } = require('../const');

//picklists
const POLICY_GENERAL_INSURANCE_TYPE = ['Personal Risk', 'Health', 'Home', 'Property', 'Contents', 'Car/Auto', 'Caravan', 'Business', 'Other'];
const POLICY_MEDICAL_COVER = ['Ambulance', 'Dental', 'Extras', 'GP', 'Hospital', 'Hospital Cash', 'Optical', 'Orthodontic', 'Private Hospital', 'Specialist and Tests'];
const POLICY_TYPE = ['Life', 'General', 'Medical'];
const POLICY_PREMIUM_FREQ = ['Weekly', 'Monthly','Quarterly', 'Half-Yearly', 'Yearly', 'Single'];
const POLICY_SOURCE = ['Addepar', 'Black Diamond', 'Xplan'];
const POLICY_STAGE = ['Did Not Proceed', 'Recommended', 'Application', 'In Force', 'Expired', 'Cancelled', 'Claim Paid', 'Declined', 'Inactive', 'Lapsed', 'Matured', 'Paid Up', 'Surrendered', 'Transferred', 'Under Claim', 'Underwriting', 'Unknown', 'Void', 'Existing', 'Renewal'];

const generate = (config, fileHandler, policyCoverage, serviceId = '') => {
    const policy = {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "Name": `${faker.random.words(5)} - Policy`,
        "practifi__Coverage_Premium__c": policyCoverage["practifi__Premium_Amount__c"],
        "practifi__Description__c": faker.lorem.words(5),
        "practifi__Entity__c": policyCoverage["practifi__Entity__c"],
        "practifi__General_Insurance_Type__c": faker.random.arrayElement(POLICY_GENERAL_INSURANCE_TYPE),
        "practifi__Medical_Cover__c": faker.random.arrayElement(POLICY_MEDICAL_COVER),
        "practifi__Premium__c": policyCoverage["practifi__Premium_Amount__c"],
        "practifi__Policy_Number__c": faker.random.alphaNumeric(15),
        "practifi__Policy_Type__c": faker.random.arrayElement(POLICY_TYPE),
        "practifi__Premium_Frequency__c": faker.random.arrayElement(POLICY_PREMIUM_FREQ),
        // "practifi__Provider__c": ,
        "practifi__Renewal_Date__c": policyCoverage["practifi__End_Date__c"],
        "practifi__Service__c": serviceId,
        "practifi__Source__c": faker.random.arrayElement(POLICY_SOURCE),
        "practifi__Stage__c": faker.random.arrayElement(POLICY_STAGE),
        "practifi__Start_Date__c": policyCoverage["practifi__Start_Date__c"],
        "practifi__Sum_Insured__c": policyCoverage["practifi__Amount__c"],
        "practifi__Service__r.Id": serviceId,
    }

    console.log('----Policy---');
    fileHandler.appendToFile(POLICY, formatData(POLICY_FIELD_LIST, policy));
    return policy.Id;
}

module.exports = generate;

/**
practifi__Coverage_Premium__c=> Currency(16, 0)
practifi__Description__c=> Text Area(255)
practifi__Entity__c=> Lookup(Entity)
practifi__General_Insurance_Type__c=> Picklist (Multi-Select)
practifi__Medical_Cover__c=> Picklist (Multi-Select)
Name=> Text(80)
practifi__Premium__c=> Currency(16, 0)
practifi__Policy_Number__c=> Text(40)
practifi__Policy_Type__c=> Picklist
practifi__Premium_Frequency__c=> Picklist
practifi__Provider__c=> Lookup(Entity)
practifi__Renewal_Date__c=> Date
practifi__Service__c=> Lookup(Service)
practifi__Source__c=> Picklist
practifi__Stage__c=> Picklist
practifi__Start_Date__c=> Date
practifi__Sum_Insured__c=> Currency(16, 0)

 * 
 */