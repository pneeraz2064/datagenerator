const faker = require('faker');
const { formatData, findConfig, filterArrayData, findFromCSV } = require('../helpers/data_helper');
const { ASSET_LIABILITY, ASSET_LIABILITY_FIELD_LIST, RECORD_TYPE } = require('../const');

const holdingGenerator = require('./holdings');
const taskGenerator = require('./task');
const eventGenerator = require('./event');
const processGenerator = require('./process');

//picklist
const SOURCE = ['Addepar', 'Black Diamond'];
const STAGE = ['Owned', 'Disposed', 'Potential', 'Application', 'Financed', 'Did Not Proceed', 'Declined'];

const CATEGORY_ASSET = ['Own home', 'Holiday home', 'Managed funds', 'Pension', 'Share portfolio', 'Funding Deposit'/*, 'Superannuation'*/];
const CATEGORY_LIABILITY = ['Credit card', 'Line of credit', 'Personal loan', 'Vehicle loan', 'Mortgage - Own home', 'Mortgage - Investment', 'Mortgage - Holiday home'];
const ASSET_RECORD_TYPE = 'Asset;practifi__Asset_Liability__c';
const LIABILITY_RECORD_TYPE = 'Liability;practifi__Asset_Liability__c';

const generate = (config, fileHandler, entity, service, isAsset) => {
    const recordType = findFromCSV(fileHandler.getOpenFile(RECORD_TYPE).stream, '$$DeveloperName$SobjectType', isAsset ? ASSET_RECORD_TYPE : LIABILITY_RECORD_TYPE);
    const assetLiability = {
        "Id": faker.datatype.uuid().replace('-', '').split('-')[0],
        "Name": `${entity.Name} ${isAsset ? 'Asset' : 'Liability'}`.substring(0, 80),
        "practifi__Account_Number__c": faker.finance.accountName() + ' - ' + faker.finance.account(),
        "practifi__Category__c": faker.random.arrayElement(isAsset ? CATEGORY_ASSET : CATEGORY_LIABILITY),
        "practifi__Client__c": entity.Id,
        "practifi__Current_Value__c": faker.datatype.number(50000),
        "practifi__Description__c": faker.random.words(20),
        "practifi__Initial_Value__c": faker.datatype.number(50000),
        "practifi__Interest_Rate__c": faker.datatype.number({ min: 2.5, max: 5.5, precision: 2 }),
        "practifi__Related_Entity__c": entity.Id,
        "practifi__Revenue__c": faker.datatype.number(50000),
        "practifi__Service__c": service.Id,
        "practifi__Source__c": '',
        "practifi__Stage__c": faker.random.arrayElement(STAGE),
        "practifi__Value__c": faker.datatype.number(50000),
        "RecordTypeId": recordType.Id || '',
        "practifi__Client__r.Id": entity.Id,
        "practifi__Related_Entity__r.Id": entity.Id,
        "practifi__Service__r.id": service.Id,
        "RecordType.$$DeveloperName$SobjectType": recordType['$$DeveloperName$SobjectType'] || ''
    }

    //TODO create a holding
    let holdingGenerated = false;
    if (isAsset && faker.datatype.boolean()) {
        holdingGenerated = true;
        const holdingToGenerate = faker.datatype.number(6);
        for (let i = 0; i <= holdingToGenerate; i++)
            holdingGenerator(config, fileHandler, assetLiability);
    } //Randomly

    if (holdingGenerated) {
        assetLiability["practifi__Value__c"] = ''
        assetLiability["practifi__Current_Value__c"] = ''
    }

    console.log('----AssetLiability---');
    fileHandler.appendToFile(ASSET_LIABILITY, formatData(ASSET_LIABILITY_FIELD_LIST, assetLiability));

    if (faker.datatype.boolean()) {
        //taskGenerator(config, fileHandler, 'AssetLiab', '', assetLiability.Id);
        // eventGenerator(config, fileHandler, 'AssetLiab', '', assetLiability.Id);
        processGenerator(config, fileHandler, entity, assetLiability.Id, '', '', service.Id);
    }

    return assetLiability;
}

module.exports = generate;
