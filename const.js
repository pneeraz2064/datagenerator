const ACCOUNT_DEFAULT_DATA_SIZE = 50000;

const ACCOUNT = 'Account';
const ACCOUNT_FIELD_LIST = [
    'Id', 'Name', 'FirstName', 'LastName', 'Salutation', 'Type', 'practifi__AUM__c', 'practifi__Became_Client_On__c',
    'practifi__Became_Prospect_On__c', 'practifi__Client_Date__c', 'practifi__Client_Stage_Entry_Date__c', 'practifi__Definitions__c',
    'practifi__Potential_AUM__c', 'practifi__Potential_Revenue__c', 'practifi__Primary_Entity_Stage__c',
    'practifi__Won_Client__c', 'practifi__Client_Stage__c', 'practifi__Type__c',
    'AccountNumber', 'AccountSource', 'RecordTypeId', 'practifi__Partner__c', 'practifi__Primary_Contact__c',
    'practifi__Primary_Member__c', 'practifi__Spouse__c', 'practifi__Primary_Entity__c', 'practifi__Primary_Entity__pc',
    'IsPersonAccount', 'practifi__Client_Segment__c', 'practifi__Days_Since_Last_Contact_Range__c', 'practifi__Last_Contact_Date__c',
    'practifi__Partner__r.Id', 'practifi__Primary_Contact__r.Id', 'practifi__Primary_Member__r.Id',
    'practifi__Spouse__r.Id', 'practifi__Primary_Entity__r.Id', 'practifi__Primary_Entity__pr.Id', 'RecordType.$$DeveloperName$SobjectType'
];
const ACCOUNT_GENERATE_TYPE = ['JUST_ACCOUNT', 'ACCOUNT_WITH_HOUSEHOLD', 'MULTIPLE_ACCCOUNT_WITH_HOUSEHOLD'];

//contact
const CONTACT = 'Contact'; //
const CONTACT_FIELD_LIST = [
    'Id', 'Name', 'FirstName', 'LastName', 'Salutation', 'IsPersonAccount',
    'AccountId', 'practifi__Alternate_Email__c', 'AssistantName', 'AssistantPhone',
    'practifi__Birth_Name__c', 'Birthdate', 'practifi__Citizenship_Status__c',
    'practifi__Country_Of_Citizenship__c', 'practifi__Country_of_Origin__c',
    'Email', 'practifi__Employer__c', 'practifi__Gender__c', 'LeadSource',
    'practifi__Marital_Status__c', 'MobilePhone', 'practifi__Preferred_Phone__c',
    'practifi__Primary_Entity__c', 'Title', 'Account.Id', 'practifi__Primary_Entity__r.Id'
];

//individual -  Not Used
const INDIVIDUAL = 'Individual';
const INDIVIDUAL_FIELD_LIST = [
    'Id', 'Name', 'FirstName', 'LastName', 'Salutation', 'BirthDate', 'ChildrenCount', 'ChildrenCount', 'Website'
]

//contact point phone
const CONTACT_POINT_PHONE = 'ContactPointPhone'; //
const CONTACT_POINT_PHONE_FIELD_LIST = [
    'Id', 'practifi__Contact__c', 'practifi__Description__c', 'ExtensionNumber', 'FormattedInternationalPhoneNumber',
    'FormattedInternationalPhoneNumber', 'IsBusinessPhone', 'IsPersonalPhone', 'IsPrimary', 'IsSmsCapable',
    'practifi__Map_to_Record__c', 'PhoneType', 'PreferenceRank', 'TelephoneNumber', 'UsageType', 'practifi__Contact__r.Id'
]
//contact point address
const CONTACT_POINT_ADDRESS = 'ContactPointAddress'; //
const CONTACT_POINT_ADDRESS_FIELD_LIST = [
    'Id', 'Name', 'Street', 'City', 'State', 'Country',
    'AddressType', 'practifi__Contact__c', 'ContactPointPhoneId', 'practifi__Description__c',
    'IsDefault', 'IsPrimary', 'practifi__Map_to_Record__c', 'PreferenceRank', 'TelephoneNumber', 'UsageType', 'practifi__Contact__r.Id', 'ContactPointPhone.Id'
]
//contact point email
const CONTACT_POINT_EMAIL = 'ContactPointEmail';
const CONTACT_POINT_EMAIL_FIELD_LIST = [
    'Id', 'practifi__Contact__c', 'practifi__Contact__c', 'EmailAddress', 'EmailDomain', 'IsPrimary',
    'practifi__Map_to_Record__c', 'UsageType', 'practifi__Contact__r.Id'
];

const RELATIONSHIP = 'practifi__Relationship__c';
const RELATIONSHIP_FIELD_LIST = [
    'Id', 'Name', 'practifi__Description__c', 'practifi__Relationship_Type__c', 'practifi__From_Contact__c',
    'practifi__To_Entity__c', 'practifi__Relationship_Type__r.practifi__Code__c', 'practifi__From_Contact__r.Id',
    'practifi__To_Entity__r.Id'
];

const DEAL = 'practifi__Deal__c';
const DEAL_FIELD_LIST = [
    'Id', 'Name', 'practifi__Annual_Revenue__c', 'practifi__AUM__c', 'practifi__Entity__c', 'practifi__Related_Entity__c',
    'practifi__Service__c', 'practifi__Service_Type__c', 'practifi__Stage__c', 'practifi__Close_Date__c', 'practifi__Entity__r.Id',
    'practifi__Related_Entity__r.Id', 'practifi__Service__r.Id', 'practifi__Service_Type__r.practifi__Code__c'
];

const SERVICE = 'practifi__Service__c';
const SERVICE_FIELD_LIST = [
    'Id', 'Name', 'practifi__Annual_Revenue__c', 'practifi__AUM__c', 'practifi__Client__c', 'RecordTypeId',
    'practifi__Related_Entity__c', 'practifi__Service_Type__c', 'practifi__Stage__c', 'practifi__Client__r.Id',
    'practifi__Related_Entity__r.Id', 'practifi__Service_Type__r.practifi__Code__c',
    'RecordType.$$DeveloperName$SobjectType'
];

const ASSET_LIABILITY = 'practifi__Asset_Liability__c';
const ASSET_LIABILITY_FIELD_LIST = [
    'Id', 'Name', 'practifi__Account_Number__c', 'practifi__Category__c', 'practifi__Client__c',
    'practifi__Current_Value__c', 'practifi__Description__c', 'practifi__Initial_Value__c', 'practifi__Interest_Rate__c',
    'practifi__Related_Entity__c', 'practifi__Revenue__c', 'practifi__Service__c', 'practifi__Source__c',
    'practifi__Stage__c', 'practifi__Value__c', 'RecordTypeId', 'practifi__Client__r.Id',
    'practifi__Related_Entity__r.Id', 'practifi__Service__r.id', 'RecordType.$$DeveloperName$SobjectType'
];

const HOLDING = 'practifi__Holding__c';
const HOLDING_FIELD_LIST = [
    'Id', 'Name', 'practifi__Revenue__c', 'practifi__As_At__c', 'practifi__Asset_Liability__c', 'practifi__Balance__c',
    'practifi__Class_Name__c', 'practifi__Price__c', 'practifi__Product_Code__c', 'practifi__Stage__c',
    'practifi__Unitised__c', 'practifi__Asset_Liability__r.Id'
];

//Not in use - Cannot upload
const EVENT = 'Event';
const EVENT_FIELD_LIST = [
    'Id', 'IsAllDayEvent', 'ActivityDate', 'StartDateTime', 'ActivityDateTime', 'Description', 'DurationInMinutes', 'Email', 'EndDateTime', 'Location', 'WhoId', 'Phone', 'IsPrivate',
    'WhatId', 'IsReminderSet', 'Subject', 'Type', 'Who.Id', 'What.Id'
];

const TASKS = 'Task';
const TASK_FIELD_LIST = [
    'Id', 'CallDurationInSeconds', 'Description', 'ActivityDate',
    'Email', 'WhoId', 'Phone', 'Priority', 'IsVisibleInSelfService', 'RecurrenceInterval', 'WhatId', 'IsReminderSet',
    'Status', 'Subject', 'Type', 'Who.Id', 'What.Id'
];

const POLICY_COVERAGE = 'practifi__Policy_Coverage__c';
const POLICY_COVERAGE_FIELD_LIST = [
    'Id', 'Name', 'practifi__Amount__c', 'practifi__Asset_Liability__c', 'practifi__End_Date__c', 'practifi__Entity__c', 'practifi__Insured__c', 'practifi__Person_Insured__c',
    'practifi__Policy__c', 'practifi__Policy_Number__c', 'practifi__Premium_Amount__c', 'practifi__Premium_Frequency__c', 'practifi__Related_Entity__c',
    'practifi__Start_Date__c', 'practifi__Type__c', 'practifi__Asset_Liability__r.Id', 'practifi__Entity__r.Id', 'practifi__Insured__r.Id', 'practifi__Person_Insured__r.Id',
    'practifi__Policy__R.id', 'practifi__Related_Entity__r.Id'
];

const POLICY = 'practifi__Policy__c';
const POLICY_FIELD_LIST = [
    'Id', 'Name', 'practifi__Coverage_Premium__c', 'practifi__Description__c', 'practifi__Entity__c', 'practifi__General_Insurance_Type__c',
    'practifi__Medical_Cover__c', 'practifi__Premium__c', 'practifi__Policy_Number__c', 'practifi__Policy_Type__c', 'practifi__Premium_Frequency__c',
    'practifi__Renewal_Date__c', 'practifi__Service__c', 'practifi__Source__c', 'practifi__Stage__c', 'practifi__Start_Date__c',
    'practifi__Sum_Insured__c', 'practifi__Service__r.Id'
];

const PROCESS = 'practifi__Process__c';
const PROCESS_FIELD_LIST = [
    'Id', 'Name', 'practifi__Asset_Liability__c', 'practifi__Completed_Date__c', 'practifi__Completed_Datetime__c', 'practifi__Deal__c',
    'practifi__Description__c', 'practifi__Due_Date__c', 'practifi__Entity__c', 'practifi__Person__c', 'practifi__Process_Type__c',
    'practifi__Related_Client__c', 'practifi__Service__c', 'practifi__Stage__c', 'practifi__Stage_Entry_Date__c',
    'practifi__Started_Date__c', 'practifi__Asset_Liability__r.Id', 'practifi__Deal__r.Id', 'practifi__Entity__r.Id', 'practifi__Person__r.Id',
    'practifi__Related_Client__r.Id', 'practifi__Service__r.Id'
];

const REFERENCE_DOCUMENT = 'practifi__Reference_Document__c';
const REFERENCE_DOCUMENT_FIELD_LIST = [
    'Id', 'Name', 'practifi__Description__c', 'practifi__Document_Number__c', 'practifi__Document_URL__c', 'practifi__Entity__c',
    'practifi__Expiration_Date__c', 'practifi__Issue_Date__c', 'practifi__Issuing_Authority__c', 'practifi__Person__c', 'practifi__Points__c',
    'practifi__Related_Entity__c', 'practifi__Type__c', 'practifi__Entity__r.Id', 'practifi__Person__r.Id', 'practifi__Related_Entity__r.Id'
]

//TODO Tasks, policies

//Readonly files
const DEAL_SERVICE_CATEGORY = 'practifi__Category__c';
const RELATIONSHIP_TYPE = 'practifi__Relationship_Type__c';
const RECORD_TYPE = 'RecordType';
const PROCESS_STAGE = 'practifi__Process_Stage__c';
const PROCESS_TYPE = 'practifi__Process_Type__c';
const PROCESS_TASK = 'practifi__Process_Task__c';

const READ_ONLY_FILES = [RECORD_TYPE, RELATIONSHIP_TYPE, DEAL_SERVICE_CATEGORY, PROCESS_STAGE, PROCESS_TYPE, PROCESS_TASK];

const DATES_START_END = ['10/07/1993', '10/07/2021'];

module.exports = {
    ACCOUNT, ACCOUNT_FIELD_LIST,
    CONTACT, CONTACT_FIELD_LIST,
    ACCOUNT_GENERATE_TYPE, ACCOUNT_DEFAULT_DATA_SIZE,
    CONTACT_POINT_PHONE, CONTACT_POINT_PHONE_FIELD_LIST,
    CONTACT_POINT_ADDRESS, CONTACT_POINT_ADDRESS_FIELD_LIST,
    CONTACT_POINT_EMAIL, CONTACT_POINT_EMAIL_FIELD_LIST,
    RELATIONSHIP, RELATIONSHIP_FIELD_LIST,
    DEAL, DEAL_FIELD_LIST,
    SERVICE, SERVICE_FIELD_LIST,
    ASSET_LIABILITY, ASSET_LIABILITY_FIELD_LIST,
    HOLDING, HOLDING_FIELD_LIST,
    EVENT, EVENT_FIELD_LIST,
    TASKS, TASK_FIELD_LIST,
    POLICY_COVERAGE, POLICY_COVERAGE_FIELD_LIST,
    POLICY, POLICY_FIELD_LIST,
    PROCESS, PROCESS_FIELD_LIST,
    REFERENCE_DOCUMENT, REFERENCE_DOCUMENT_FIELD_LIST,
    RELATIONSHIP_TYPE,
    RECORD_TYPE,
    DEAL_SERVICE_CATEGORY,
    READ_ONLY_FILES,
    DATES_START_END,
    PROCESS_STAGE,
    PROCESS_TYPE,
    PROCESS_TASK,
}
