const express = require('express');
const faker = require('faker');
const { READ_ONLY_FILES } = require('./const');
const { findConfig } = require('./helpers/data_helper');

const generateAccount = require('./generators/account');
const file_handler = require('./helpers/file_handler');

const app = express();

app.get('/', (req, res) => {
    res.json({ message: 'alive' });
});

app.get('/generate', (req, res) => {
    //First initialize the readonly files
    let openedFilesCount = 0;
    // const CONFIG = req.body.config;

    const CONFIG = findConfig(req, ['body', 'config']) || {};
    for (const readOnlyFile of READ_ONLY_FILES) {
        file_handler.openFile(readOnlyFile, true, null, (err) => {
            if (err) {
                console.log('Error ocurred while opening file :', readOnlyFile, err);
                return;
            }
            openedFilesCount++;
            if (openedFilesCount == READ_ONLY_FILES.length) {
                generateAccount(CONFIG, file_handler);
                file_handler.closeAllFiles();
                console.log('Finished generating files');
            }
        });
    }

    res.send('<h1>Generation started. Please check the logs for more details.</h1>')
});

app.listen(3000)